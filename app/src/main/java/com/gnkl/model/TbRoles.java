package com.gnkl.model;

import java.io.Serializable;

/**
 * Created by jmejia on 21/12/16.
 */

public class TbRoles implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer idRol;
    private String desRol;

//    @ManyToMany(mappedBy = "roles")
//    private Set<TbUsuarios> usuarios;

    public TbRoles() {
    }

    public TbRoles(Integer idRol) {
        this.idRol = idRol;
    }

    public TbRoles(Integer idRol, String desRol) {
        this.idRol = idRol;
        this.desRol=desRol;
    }

    public Integer getIdRol() {
        return idRol;
    }

    public void setIdRol(Integer idRol) {
        this.idRol = idRol;
    }

    public String getDesRol() {
        return desRol;
    }

    public void setDesRol(String desRol) {
        this.desRol = desRol;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRol != null ? idRol.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbRoles)) {
            return false;
        }
        TbRoles other = (TbRoles) object;
        if ((this.idRol == null && other.idRol != null) || (this.idRol != null && !this.idRol.equals(other.idRol))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.com.gnkl.novartis.model.TbRoles[ idRol=" + idRol + " ]";
    }

}

