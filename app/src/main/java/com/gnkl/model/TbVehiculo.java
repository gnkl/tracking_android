package com.gnkl.model;

import java.io.Serializable;

/**
 * Created by jmejia on 22/12/16.
 */

public class TbVehiculo implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    private String noEconomico;
    private String descripcion;
    private String placas;
    private String noSerie;
    private String marca;
    private String submarca;
    private Integer capCarga;

    public TbVehiculo() {
    }

    public TbVehiculo(Integer id) {
        this.id = id;
    }

    public TbVehiculo(Integer id, String noEconomico) {
        this.id = id;
        this.noEconomico = noEconomico;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNoEconomico() {
        return noEconomico;
    }

    public void setNoEconomico(String noEconomico) {
        this.noEconomico = noEconomico;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbVehiculo)) {
            return false;
        }
        TbVehiculo other = (TbVehiculo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.saa.lerma.mavenproject2.TbVehiculo[ id=" + id + " ]";
    }

    /**
     * @return the placas
     */
    public String getPlacas() {
        return placas;
    }

    /**
     * @param placas the placas to set
     */
    public void setPlacas(String placas) {
        this.placas = placas;
    }

    /**
     * @return the noSerie
     */
    public String getNoSerie() {
        return noSerie;
    }

    /**
     * @param noSerie the noSerie to set
     */
    public void setNoSerie(String noSerie) {
        this.noSerie = noSerie;
    }

    /**
     * @return the marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * @param marca the marca to set
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * @return the submarca
     */
    public String getSubmarca() {
        return submarca;
    }

    /**
     * @param submarca the submarca to set
     */
    public void setSubmarca(String submarca) {
        this.submarca = submarca;
    }

    /**
     * @return the capCarga
     */
    public Integer getCapCarga() {
        return capCarga;
    }

    /**
     * @param capCarga the capCarga to set
     */
    public void setCapCarga(Integer capCarga) {
        this.capCarga = capCarga;
    }


}
