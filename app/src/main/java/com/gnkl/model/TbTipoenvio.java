package com.gnkl.model;

import java.io.Serializable;

/**
 * Created by jmejia on 22/12/16.
 */

public class TbTipoenvio implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer idEnvio;
    private String desEnvio;

    public TbTipoenvio() {
    }

    public TbTipoenvio(Integer idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Integer getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Integer idEnvio) {
        this.idEnvio = idEnvio;
    }

    public String getDesEnvio() {
        return desEnvio;
    }

    public void setDesEnvio(String desEnvio) {
        this.desEnvio = desEnvio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbTipoenvio)) {
            return false;
        }
        TbTipoenvio other = (TbTipoenvio) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.com.gnkl.novartis.model.TbTipoenvio[ idEnvio=" + idEnvio + " ]";
    }

}
