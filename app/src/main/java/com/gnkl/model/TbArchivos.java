package com.gnkl.model;

import java.io.Serializable;

/**
 * Created by jmejia on 22/12/16.
 */

public class TbArchivos implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer idArchivo;
    private String pathArchivo;

    public TbArchivos() {
    }

    public TbArchivos(Integer idArchivo) {
        this.idArchivo = idArchivo;
    }

    public TbArchivos(Integer idArchivo, String pathArchivo) {
        this.idArchivo = idArchivo;
        this.pathArchivo = pathArchivo;
    }

    public Integer getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(Integer idArchivo) {
        this.idArchivo = idArchivo;
    }

    public String getPathArchivo() {
        return pathArchivo;
    }

    public void setPathArchivo(String pathArchivo) {
        this.pathArchivo = pathArchivo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idArchivo != null ? idArchivo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbArchivos)) {
            return false;
        }
        TbArchivos other = (TbArchivos) object;
        if ((this.idArchivo == null && other.idArchivo != null) || (this.idArchivo != null && !this.idArchivo.equals(other.idArchivo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.com.gnkl.novartis.model.TbArchivos[ idArchivo=" + idArchivo + " ]";
    }

}
