package com.gnkl.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sourcey.materiallogindemo.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

/**
 * Created by jmejia on 22/12/16.
 */

public class HistoricoItemAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<Map<String,Object>> mDataSource;

    public HistoricoItemAdapter(Context context, ArrayList<Map<String,Object>> items) {
        mContext = context;
        mDataSource = items;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    //1
    @Override
    public int getCount() {
        return mDataSource.size();
    }

    //2
    @Override
    public Object getItem(int position) {
        return mDataSource.get(position);
    }

    //3
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Map<String,Object> recipe = (Map) getItem(position);
        View rowView = mInflater.inflate(R.layout.list_item_recipe, parent, false);
        TextView titleTextView = (TextView) rowView.findViewById(R.id.recipe_list_title);
        TextView subtitleTextView = (TextView) rowView.findViewById(R.id.recipe_list_subtitle);
        TextView detailTextView = (TextView) rowView.findViewById(R.id.recipe_list_detail);
        titleTextView.setText((String) recipe.get("des_status"));
        Long time = (Long) recipe.get("fecha");
        Date date = new Date(time * 1000);
        subtitleTextView.setText(date.toString());
        Integer km = (Integer) recipe.get("kilometraje");
        detailTextView.setText(km.toString());
        return rowView;
    }

}
