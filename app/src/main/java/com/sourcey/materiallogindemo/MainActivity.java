package com.sourcey.materiallogindemo;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.gnkl.adapter.BitacoraItemAdapter;
import com.gnkl.model.TbBitacora;
import com.gnkl.model.TbUsuarios;
import com.gnkl.singleton.Session;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.Bind;


public class MainActivity extends ActionBarActivity {

    @Bind(R.id.bitacora_list_view) ListView _bitacoraListView;
    ArrayList<Map<String, Object>>  listBitacoraRecoleccion = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("tag ","on crearte method");

        //Intent intent = new Intent(this, LoginActivity.class);
        //startActivity(intent);
        Log.d("main activity","");
        new BitacoraEntregaTask(this).execute();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class BitacoraEntregaTask extends AsyncTask<Void, Void, ArrayList<Map<String, Object>>> {
        Context ctx;
        BitacoraEntregaTask(Context ctx) {
            this.ctx =ctx;
        }

        @Override
        protected ArrayList<Map<String, Object>> doInBackground(Void... params) {
            try {
                Session session = Session.getInstance();
                TbUsuarios loggedUser = session.getLoggedUser();

                final String url = getString(R.string.base_uri)+"/android/list_bitacora_tipo_operador?iduser=13&tipo=1";
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                ArrayList<Map<String, Object>> result = restTemplate.getForObject(url,ArrayList.class);
                return result;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<Map<String, Object>> listResult) {
            if(listResult!=null && !listResult.isEmpty()){
                listBitacoraRecoleccion = listResult;
                _bitacoraListView = (ListView) findViewById(R.id.bitacora_list_view);
                BitacoraItemAdapter adapter = new BitacoraItemAdapter(this.ctx, listResult);
                _bitacoraListView.setAdapter(adapter);
                _bitacoraListView.setOnItemClickListener(new ListClickHandler());
            }

        }

    }


    public class ListClickHandler implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> adapter, View view, int position, long arg3) {
            // TODO Auto-generated method stub
            Log.d("position",String.valueOf(position));
            if(listBitacoraRecoleccion!=null && listBitacoraRecoleccion.size()>position){
                Map<String,Object> item = listBitacoraRecoleccion.get(position);
                Intent intent = new Intent(MainActivity.this, DetalleBitacoraActivity.class);
                Integer id = (Integer) item.get("id");
                intent.putExtra("selected-item", id.toString());
                startActivity(intent);
                Log.d("position",(String)item.get("nombre"));
            }
        }
    }

}
