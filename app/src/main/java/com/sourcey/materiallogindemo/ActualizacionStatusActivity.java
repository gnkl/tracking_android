package com.sourcey.materiallogindemo;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

import butterknife.Bind;

public class ActualizacionStatusActivity extends AppCompatActivity {

    private String _idBitacora = null;
    private Map<String, Object> _resultLastStatus = null;
    private ImageView imgView;
    private Button upload;
    private Bitmap bitmap;
    private ProgressDialog dialog;

    @Bind(R.id.comment) EditText _observacionesText;
    @Bind(R.id.new_kilometraje) EditText _kilometrajeText;
    @Bind(R.id.update_status) Button _updateStatusButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actualizacion_status);

        Bundle bundle = getIntent().getExtras();
        String id = bundle.getString("selected-item");
        if(id!=null && !id.isEmpty()){
            _idBitacora=id;
            new CurrentStatusTask(this).execute();
        }

        _updateStatusButton = (Button) findViewById(R.id.update_status);
        _updateStatusButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                updateStatusProccess();
            }
        });

        imgView = (ImageView) findViewById(R.id.imageView);
        upload = (Button) findViewById(R.id.addImage);

        upload.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if (bitmap == null) {
                    Toast.makeText(getApplicationContext(),
                            "Please select image", Toast.LENGTH_SHORT).show();
                } else {
                    dialog = ProgressDialog.show(ActualizacionStatusActivity.this, "Uploading", "Please wait...", true);
                    //new ImageUploadTask().execute();
                }
            }
        });


        Log.d("id ",_idBitacora);
    }

    private class CurrentStatusTask extends AsyncTask<Void, Void, Map<String, Object>> {
        Context ctx;
        CurrentStatusTask(Context ctx) {
            this.ctx =ctx;
        }

        @Override
        protected Map<String, Object> doInBackground(Void... params) {
            try {

                final String url = getString(R.string.base_uri)+"/android/current_status?idbitacora="+_idBitacora;
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                Map<String, Object> result = restTemplate.getForObject(url,Map.class);
                return result;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Map<String, Object> mapResult) {
            if(mapResult!=null && !mapResult.isEmpty()){
                Log.d("list result",mapResult.toString());
                _resultLastStatus = mapResult;
                TextView idBitacoraText = (TextView) findViewById(R.id.new_status);
                idBitacoraText.setText((String) mapResult.get("des_status"));

                TextView nombreBitacoraText = (TextView) findViewById(R.id.km_actual);
                Integer currentKm = (Integer) mapResult.get("current_km");
                nombreBitacoraText.setText(currentKm.toString() );
            }else{
                _resultLastStatus = null;
            }
        }
    }

    public void updateStatusProccess() {
        Log.d("login", "Login");

        if (!validate()) {
            return;
        }

        Log.d("logic","send data to the server");

        new UpdateStatusTask(this).execute();

        /*_loginButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        // TODO: Implement your own authentication logic here.

        String url = "http://192.168.0.50:8080/WallmartTracking/login?username=sistemas&password=T3mporal.2016";

        new HttpRequestTask(this).execute();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed
                        onLoginSuccess();
                        // onLoginFailed();
                        progressDialog.dismiss();
                    }
                }, 3000);*/
    }

    public boolean validate() {
        boolean valid = true;

        _observacionesText = (EditText) findViewById(R.id.comment);
        _kilometrajeText = (EditText) findViewById(R.id.new_kilometraje);

        String observaciones = _observacionesText.getText().toString();
        String kilometrajeTxt = _kilometrajeText.getText().toString();
        Integer km = 0;
        if(kilometrajeTxt!=null && !kilometrajeTxt.isEmpty()){
            km = new Integer(kilometrajeTxt);
        }
        //
        Log.d("km",kilometrajeTxt);
        Integer currentKm = (Integer) _resultLastStatus.get("current_km");

        if (observaciones.isEmpty()) {
            _observacionesText.setError("enter a valid comment");
            valid = false;
        } else {
            _observacionesText.setError(null);
        }


        if (km > 0 && km < currentKm) {
            _kilometrajeText.setError("el kilometraje debe ser mayor a: "+currentKm);
            valid = false;
        } else {
            _kilometrajeText.setError(null);
        }
        return valid;
    }

    private class UpdateStatusTask extends AsyncTask<Void, Void, Map<String, Object>> {
        Context ctx;
        String comentarios="";
        Integer kilometraje=0;

        UpdateStatusTask(Context ctx) {
            this.ctx =ctx;
        }

        @Override
        protected void onPreExecute() {
            _observacionesText = (EditText) findViewById(R.id.comment);
            _kilometrajeText = (EditText) findViewById(R.id.new_kilometraje);

            comentarios = _observacionesText.getText().toString();
            String kilometrajeTxt = _kilometrajeText.getText().toString();
            if(kilometrajeTxt!=null && !kilometrajeTxt.isEmpty()){
                kilometraje = new Integer(kilometrajeTxt);
            }


        }

        @Override
        protected Map<String, Object> doInBackground(Void... params) {
            try {
                final String url = getString(R.string.base_uri)+"/android/update_status?idbitacora="+_idBitacora+"&comentario="+comentarios+"&km="+kilometraje;
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                Map<String, Object> result = restTemplate.getForObject(url,Map.class);
                return result;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Map<String, Object> mapResult) {
            Intent intent = new Intent(ActualizacionStatusActivity.this, DetalleBitacoraActivity.class);
            intent.putExtra("selected-item", _idBitacora.toString());
            startActivity(intent);
            finish();
        }
    }

    class ImageUploadTask extends AsyncTask <Void, Void, String>{
        @Override
        protected String doInBackground(Void... unsued) {
            try {
                return "";
            } catch (Exception e) {
                if (dialog.isShowing())
                    dialog.dismiss();
                Toast.makeText(getApplicationContext(), "mal", Toast.LENGTH_LONG).show();
                Log.e(e.getClass().getName(), e.getMessage(), e);
                return null;
            }

            // (null);
        }
    }
}
