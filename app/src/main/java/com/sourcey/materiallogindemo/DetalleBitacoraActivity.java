package com.sourcey.materiallogindemo;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gnkl.adapter.BitacoraItemAdapter;
import com.gnkl.adapter.HistoricoItemAdapter;
import com.gnkl.model.TbBitacora;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Map;

import butterknife.Bind;

public class DetalleBitacoraActivity extends AppCompatActivity {

    private String idBitacora=null;
    @Bind(R.id.list_historico) ListView _historicoListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_bitacora);

        Bundle bundle = getIntent().getExtras();
        String id = bundle.getString("selected-item");
        if(id!=null && !id.isEmpty()){
            idBitacora=id;
            new BitacoraTask(this).execute();
            new HistoricoBitacoraTask(this).execute();
        }
        Log.d("id ",idBitacora);

        //TODO ocultar si es null bitacora
        Button button = (Button) findViewById(R.id.buttonActStatus);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d("test","test");
                Intent intent = new Intent(DetalleBitacoraActivity.this, ActualizacionStatusActivity.class);
                intent.putExtra("selected-item", idBitacora);
                startActivity(intent);
            }
        });

    }

    private class BitacoraTask extends AsyncTask<Void, Void, TbBitacora> {
        Context ctx;
        BitacoraTask(Context ctx) {
            this.ctx =ctx;
        }

        @Override
        protected TbBitacora doInBackground(Void... params) {
            try {
                final String url = getString(R.string.base_uri)+"/android/get_bitacora?idbitacora="+idBitacora;
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                TbBitacora result = restTemplate.getForObject(url,TbBitacora.class);
                return result;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(TbBitacora selectedBitacora) {
            if(selectedBitacora!=null ){
                Log.d("bitacora ",selectedBitacora.getNombre());
                Log.d("bitacora id",selectedBitacora.getId()+" ");

                TextView idBitacoraText = (TextView) findViewById(R.id.id_bitacora);
                //System.out.println(idBitacoraText);
                idBitacoraText.setText(selectedBitacora.getId().toString());

                TextView nombreBitacoraText = (TextView) findViewById(R.id.nombre_bitacora);
                nombreBitacoraText.setText(selectedBitacora.getNombre());

                TextView operadorBitacoraText = (TextView) findViewById(R.id.operador);
                operadorBitacoraText.setText(selectedBitacora.getIdOperador().getNombre()+" "+selectedBitacora.getIdOperador().getApellidoPat()+" "+selectedBitacora.getIdOperador().getApellidoMat());

                TextView tipoBitacoraText = (TextView) findViewById(R.id.tipo);
                tipoBitacoraText.setText(selectedBitacora.getIdEnvio().getDesEnvio());

                TextView estatusBitacoraText = (TextView) findViewById(R.id.estatus);
                estatusBitacoraText.setText(selectedBitacora.getIdStatus().getDesStatus());

            }

        }

    }

    private class HistoricoBitacoraTask extends AsyncTask<Void, Void, ArrayList<Map<String,Object>>> {
        Context ctx;

        HistoricoBitacoraTask(Context ctx) {
            this.ctx =ctx;
        }

        @Override
        protected ArrayList<Map<String,Object>> doInBackground(Void... params) {
            try {
                final String url = getString(R.string.base_uri)+"/android/historico_bitacora?idbitacora="+idBitacora;
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                ArrayList<Map<String,Object>> result = restTemplate.getForObject(url,ArrayList.class);
                return result;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<Map<String,Object>> response) {
            if(response != null ){
                Log.d("bitacora ",response.toString());
                System.out.println(response);
                _historicoListView = (ListView) findViewById(R.id.list_historico);
                HistoricoItemAdapter adapter = new HistoricoItemAdapter(this.ctx, response);
                _historicoListView.setAdapter(adapter);
            }
        }
    }


    public void onClickBtn(View v) {
        Toast.makeText(this, "Clicked on Button", Toast.LENGTH_LONG).show();
    }


}
